package com.example.demo.service;

import com.example.demo.controller.dto.UserDTO;
import com.example.demo.persistence.model.User;
import com.example.demo.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;


    public List<UserDTO> findAll() {
        return userRepository.findAll()
                .stream().map(u -> new UserDTO(u.getUsername(), u.getEmail(), u.getUrlProfile()))
                .collect(Collectors.toList());
    }

    public UserDTO save(UserDTO userDTO) {
        userRepository.save(new User(userDTO.getUsername(), userDTO.getEmail(), userDTO.getUrlProfile()));
        return userDTO;
    }

    public UserDTO update(Long id, UserDTO userDTO) {
        User user = new User(userDTO.getUsername(), userDTO.getEmail(), userDTO.getUrlProfile());
        user.setId(id);

        userRepository.save(user);

        return userDTO;
    }

    public UserDTO find(Long id) {
        Optional<User> optionalUser = userRepository.findById(id);

        if (optionalUser.isEmpty()) {
            throw new RuntimeException("Id invalido");
        }

        User user = optionalUser.get();

        return new UserDTO(user.getUsername(), user.getEmail(), user.getUrlProfile());
    }

    public void delete(Long id){
        userRepository.deleteById(id);
    }
}
