FROM openjdk:11-slim AS builder
ENV APP_HOME=/usr/app/
WORKDIR $APP_HOME
COPY . .
RUN ./mvnw install

FROM openjdk:11-slim AS docker
LABEL maintainer="r.batilli@gmail.com"
LABEL "service"="API Rest"
COPY --from=builder /usr/app/target/*.jar app.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-jar","app.jar"]
EXPOSE 8080
